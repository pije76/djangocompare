# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.item import Item, Field
from scrapy_djangoitem import DjangoItem
from querydata.models import Author


class BookItem(scrapy.Item):
#class BookItem(DjangoItem):
#    "fields for this item are automatically created from the django model"
#    django_model = Author
#    pass
    title = scrapy.Field()
    price = scrapy.Field()
    author = scrapy.Field()
#    pid=scrapy.Field()
#    name=scrapy.Field()
#    link=scrapy.Field()
#    description=scrapy.Field()
#    location=scrapy.Field()
#    category=scrapy.Field()
