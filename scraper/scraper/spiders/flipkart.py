import scrapy
import re

from scrapy.crawler import CrawlerProcess, Crawler
from scrapy.conf import settings as scrapy_settings
from scrapy.selector import Selector

from querydata.models import Book
from querydata.views import *

from scraper.items import BookItem


class FlipkartSpider(scrapy.Spider):
    name = "flipkart"
    allowed_domains = ["http://flipkart.com/"]
    start_urls = []
#    title = ''

    def __init__(self, title):
        self.title = title
        url = "https://www.flipkart.com/books/pr?sid=bks&q="
        self.start_urls = [url]
        return title
