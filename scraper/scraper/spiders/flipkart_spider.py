import scrapy
import re

from scrapy.crawler import CrawlerProcess, Crawler
from scrapy.conf import settings as scrapy_settings
from scrapy.selector import Selector

from querydata.models import Book
from querydata.views import *

from scraper.items import BookItem


class FlipkartSpider(scrapy.Spider):
    name = "flipkart"
    allowed_domains = ["http://flipkart.com/"]
    start_urls = []
#    title = ''

    def __init__(self, title):
        self.title = title
        url = "https://www.flipkart.com/books/pr?sid=bks&q="
        self.start_urls = [url]
        return title

import scrapy
from scraper.items import BookItem
from scrapy.selector import Selector


class FlipkartDataSpider(scrapy.Spider):
    name = 'flipkart_spider'
    start_urls = ['https://www.flipkart.com/books/pr?sid=bks&q=harry+potter']

    def parse(self, response):
        hxs = Selector(response)
        item = BookItem()
        item['author_name'] = hxs.select('//a[@class="_2cLu-l"]/@title').extract()
        yield item

class FlipkartSpider(scrapy.Spider):
    name = 'flipkart_spider'
    start_urls = ['http://www.flipkart.com/search?q=moto+g3']

    def parse(self, response):
        div = response.xpath('//div[@class="gd-row browse-grid-row"]')[0]
        results = div.xpath('.//div[@class="gd-col gu3"]')
        for result in results:
            try:
                item = PriceScraperItem()
                item['name'] = result.xpath('.//a[@data-tracking-id="prd_title"]/@title').extract()[0]
                item['price'] = result.xpath('.//div[@class="pu-price"]')[0].xpath('.//span/text()').extract()[0]
                yield item
            except:
                pass
