from django.conf.urls import *
from views import *


urlpatterns = [
#    url(r'^$', SearchFormView.as_view(), name='home'),
#    url(r'^$', MainView.as_view(), name='home'),
    url(r'^$', home, name='home'),
#    url(r'^$', CountryAutocomplete.as_view(), name='home'),
#    url(r'^searchbar/$', SearchBarView.as_view(), name='searchbar'),
#    url(r'^search/$', BookFormView.as_view(), name='search'),
    url(r'^search/$', search, name='search'),
#    url(r'^search/(?P<keyword>[a-zA-Z0-9!@#$&()\\ -`.+,/\"]*)/(?P<page>[0-9]*)/$', SearchResultView.as_view(), name='search'),
    #url(r'^search/(?P<title>[a-zA-Z0-9!@#$&()\\ -`.+,/\"]*)/(?P<author>[a-zA-Z0-9!@#$&()\\ -`.+,/\"]*)/$', SearchResultView.as_view(), name='search'),
    #url(r'^search/(?P<keyword>[a-zA-Z0-9!@#$&()\\ -`.+,/\"]*)/(?P<page>[0-9]*)/$', search, name='search'),
    #url(r'^search/(?P<title>\w+)/(?P<author_name>\w+)/$', search, name='search'),
    url(r'^product/(?P<id>[a-zA-Z0-9!@#$&()\\ -`.+,/\"]*)/$', ProductView.as_view(), name='product'),
    #url(r'^product/(?P<slug>[-\w]+)$', ProductView.as_view(), name='product'),
    url(r'^faq/$', FAQView.as_view(), name='faq'),
    url(r'^contact/$', ContactView.as_view(), name='contact'),
    url(r'^about/$', AboutView.as_view(), name='about'),
    #url(r'^robots.txt/$',RobotsView.as_view(content_type='text/plain')),
]
