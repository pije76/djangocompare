from django.core.management.base import BaseCommand
from django.conf import settings

from bs4 import BeautifulSoup
try:
    from urllib.request import urlopen
    from urllib import request
except ImportError:
    from urllib2 import urlopen, Request
from querydata.models import Actor


men_list = 'http://www.imdb.com/list/ls050274118/'
women_list = 'http://www.imdb.com/list/ls000055475/'

class Command(BaseCommand):
    help = 'Scraping data through Django command'


    def handle(self, *args, **options):
        def fetch_data_and_populate(imdb_list_url, gender):
            html = urlopen(imdb_list_url).read()
            soup = BeautifulSoup(html, 'html.parser')
            # A list with BeautifulSoup items to iterate over
            actor_items = soup.findAll('div', {'class': 'list_item'})
            new_actors = []
            for actor in actor_items:
                # Finds .list_item b a and its content
                name = actor.b.a.text
                # Finds .list_item's img tag and its source
                img_url = actor.img['src']
                # Create a new Actor object
                new_actors.append(Actor(name=name, img_url=img_url, gender=gender))

            # Save all models in 1 query instead of 100
            Actor.objects.bulk_create(new_actors)

        fetch_data_and_populate(men_list, 'M')
        fetch_data_and_populate(women_list, 'F')
