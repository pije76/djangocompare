from django.contrib import admin

from .models import *

# Register your models here.
class AuthorAdmin(admin.ModelAdmin):
    list_display = ('author_name',)
    search_fields = ['author_name']
    list_per_page = 25

class BookAdmin(admin.ModelAdmin):
#    list_display = ('title', 'short_description', 'avg_rat', 'rating_count', 'num_page', 'pub_date', 'pub', 'link')
    list_display = ('title',)
    search_fields = ['title']
    list_per_page = 25

class VendorAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ['name']
    list_per_page = 25

class EmployeeAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name')
    search_fields = ['first_name', 'last_name']

class RatingAdmin(admin.ModelAdmin):
    list_display = ('five_r', 'four_r', 'three_r', 'two_r', 'one_r', 'total_r')
    search_fields = ['total_r']

class SimilarBookAdmin(admin.ModelAdmin):
    list_display = ('id_o', 'id_d')
    search_fields = ['id_o', 'id_d']

admin.site.register(Author, AuthorAdmin)
admin.site.register(Book, BookAdmin)
admin.site.register(Vendor, VendorAdmin)
admin.site.register(Employee, EmployeeAdmin)
admin.site.register(Rating, RatingAdmin)
admin.site.register(SimilarBook, SimilarBookAdmin)
