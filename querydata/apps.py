from django.apps import AppConfig


class QuerydataConfig(AppConfig):
    name = 'querydata'
