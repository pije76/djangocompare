import datetime
from haystack import indexes
from models import *

class AuthorIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    author_name = indexes.CharField(model_attr='author_name')

    def get_model(self):
        return Author

    def index_queryset(self, using=None):
#        return self.get_model().objects.filter()
        return self.get_model().objects.all()

class BookIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')

    def get_model(self):
        return Book

    def index_queryset(self, using=None):
#        return self.get_model().objects.filter()
        return self.get_model().objects.all()
