from django import forms
from django.conf import settings
from django.contrib import messages
from django.core import serializers
from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext
from django.views.generic import TemplateView, FormView

import re
import os
import operator
import json
import scrapy

from bs4 import BeautifulSoup
from dal import autocomplete
from haystack.generic_views import SearchView, FacetedSearchView
from itertools import chain
from operator import attrgetter
from math import ceil

try:
    from urllib.request import urlopen
    from urllib import request
except ImportError:
    from urllib2 import urlopen, Request

from scrapy.conf import settings as scrapy_settings
from scrapy.crawler import CrawlerProcess, Crawler, CrawlerRunner
from scrapy.utils.project import get_project_settings
from scrapy.settings import Settings
from scrapy import log, signals

from twisted.internet import reactor

from scraper import spiders
from scraper.spiders import *
from scraper.spiders import flipkart
from scraper.spiders.flipkart import FlipkartSpider

#from spiders.run_spiders import run_spiders


from models import *
from forms import *

#from queryapi.amazon_reviews import *
#from queryapi.flipkart_reviews import *
#from queryapi.snapdeal_reviews import *


#def normalize_query(query_string,
#   findterms=re.compile(r'"([^"]+)"|(\S+)').findall,
#   normspace=re.compile(r'\s{2,}').sub):

#   return [normspace(' ',(t[0] or t[1]).strip()) for t in findterms(query_string)]
def run_scrapers(key, excluded):
    spider_list = run_spiders(key, excluded)
    return spider_list


def filter_list(store_query_list):

    title_list = []
    output_list = []

    for game_list in store_query_list:
        for game in game_list:
            fake_title = game['faketitle']
            if fake_title not in title_list:
                title_list.append(fake_title)
                output_list.append(game)
            else:
                for prevgame in output_list:
                    if prevgame['faketitle'] == fake_title and game['price'] < prevgame['price']:
                        output_list.remove(prevgame)
                        output_list.append(game)

    return output_list


def get_query(query_string, search_fields):

    query = None
    terms = re.compile(r'[^\s";,.:]+').findall(query_string)
    for term in terms:
        or_query = None
        for field_name in search_fields:
            q = Q(**{"%s__icontains" % field_name: term})
            if or_query is None:
                or_query = q
            else:
                or_query = or_query | q
        if query is None:
            query = or_query
        else:
            query = query & or_query
    return query


# Create your views here.
def home(request, new_actors=None):
    errors = []

#    bookform = BookForm()
#    authorform = AuthorForm()

    if request.method == 'GET':
        bookform = BookForm()
        authorform = AuthorForm()
        return render(
            request, 'index.html',
            {
                'bookform': bookform,
                'authorform': authorform,
            }
        )

    elif request.method == 'POST':
        if bookform.is_valid() or authorform.is_valid():
            return render(
                request, 'index.html',
                {
                    'bookform': bookform,
                    'authorform': authorform,
                }
            )


def search(request):
    errors = []

#    bookform = BookForm()
#    authorform = AuthorForm()
#    bookform = BookForm(request.GET or None)
#    authorform = AuthorForm(request.GET or None)
#    google = 'http://google.com/search?q='

#    if 'title' in request.GET and request.GET['title']:
#    if 'title' in request.GET:
    if request.method == 'GET':
        title = request.GET['title'].strip()

        if title or author:
#            results = Book.objects.filter(title__icontains=title).filter(author__author_name__icontains=author)
            results = Book.objects.filter(title__icontains=title)
#            if results is None:
            os.environ.setdefault("SCRAPY_SETTINGS_MODULE", "scraper.settings")

#            spider = FlipkartSpider(url='https://www.flipkart.com/books/pr?sid=bks&q=')
#            crawler_settings = get_project_settings()
#            crawler = Crawler(settings)
#            crawler = CrawlerProcess(Settings())
#            crawler = CrawlerProcess(crawler_settings)
#            crawler = CrawlerRunner(crawler_settings)
            crawler = CrawlerRunner(get_project_settings())
#            crawler = CrawlerRunner(settings.SCRAPY_SETTINGS)
#            crawler = Crawler(flipkart, settings)
            crawler.crawl(FlipkartSpider, title=title)
#            crawler.start()
#            crawler.signals.connect(reactor.stop, signal=signals.spider_closed)
#            crawler.configure()
#            crawler.crawl(spider)
#            crawler.start()
#            log.start()
#            reactor.run()

        return render(
            request, 'search/search_data.html',
            {
                'results': results,
                'title': title,
                'crawler': crawler,
            }
        )

#    elif request.method == 'POST':
#        if title.is_valid() or author.is_valid():
#        if bookform.is_valid() or authorform.is_valid():
#            results = Book.objects.filter(title__icontains=title).filter(author__author_name__icontains=author)

#            for query in results:
#            url = 'google' + query
#            header = urllib.request.Request(url, headers={'User-Agent': "Magic Browser"})
#            responsehdrs = urllib.request.urlopen(header)
#            html = responsehdrs.read()
#            soup = BeautifulSoup(html, 'html.parser')
#            book_items = soup.findAll('div', {'class': 'list_item'})
#            new_actors = []

#            for book in book_items:
#                title = book.b.a.text
#                img_url = book.img['src']
#                new_actors.append(Actor(name=name, img_url=img_url, gender=gender))

#            Actor.objects.bulk_create(new_actors)

#        search(men_list, 'M')
#        search(women_list, 'F')
#        home(google, 'new_actors')
#        message.append('Success!')
#        else:
#            errors.append('Enter a search term...')

#        return render(
#            request, 'search/search_data.html',
#            {
#                'results': results,
#                'new_actors': new_actors,
#                'message': message,
#            }
#        )


def searchdata(request):
    global cache_key
    cache_key = ''

    if request.method == 'GET':
        key = request.GET.get("title", 'r+')
        key = re.sub(r'[^\s\w]', '', key).strip()
        excluded = request.GET.get("filters")
        excluded = excluded.split(',') if excluded else None
        if key == '':
            messages.add_message(request, messages.ERROR, "You must search for something.")
            return redirect('querydata:home')

    if cache.get(cache_key+'+output_list'):
        output_list = cache.get(cache_key+'+output_list')
        store_query_list = cache.get(cache_key+'+store_query_list')
        cache_key = '?'+request.get_full_path().split('?')[-1].strip('+').replace('%20', '+').replace(',','%2C')
        basic_cache_key = cache_key.split('&filters')[0]
        if request.path.endswith('as_json'):
            return JsonResponse(output_list, safe=False)
        else:

            return render(
                request, 'search/search_data.html',
                {
                    'output_list': output_list,
                    'store_query_list': store_query_list,
                }
            )

    if excluded:
        if cache.get(basic_cache_key + '+output_list'):
            output_list = cache.get(basic_cache_key + '+output_list')
            output_list = [x for x in output_list if x['store'] not in excluded]
            store_query_list = cache.get(basic_cache_key + '+store_query_list')
            store_query_list = [x for x in store_query_list if x and x[0]['store'] not in excluded]
            if request.path.endswith('as_json'):
                return JsonResponse(output_list, safe=False)
            else:
                return render(
                    request, 'search/search_data.html',
                    {
                        'output_list': output_list,
                        'store_query_list': store_query_list,
                    }
                )

    store_query_list, offline = run_scrapers(key, excluded)

    output_list = filter_list(store_query_list)
    output_list = sorted(output_list, key=lambda k: k['title'])

    if output_list:
        if not offline:
            cache.set(cache_key+'+store_query_list', store_query_list, 60*10)
            cache.set(cache_key + '+output_list', output_list, 60 * 10)

        if request.path.endswith('as_json'):
            return JsonResponse(output_list, safe=False)

        return render(
            request, 'search/search_data.html',
            {
                'output_list': output_list,
                'store_query_list': store_query_list,
                'offline': offline,
            }
        )


class MainView(TemplateView):
    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        search_form = BookForm(self.request.GET or None)
        context = self.get_context_data(**kwargs)
        context['search_form'] = search_form
        return self.render_to_response(context)


class SearchFormView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super(SearchFormView, self).get_context_data(**kwargs)
        top_scores = (Product.objects.order_by('-clicked').values_list('clicked', flat=True).distinct())
        top_records = (Product.objects.order_by('-clicked').filter(clicked__in=top_scores[:5]))
        context['products'] = top_records[:5]
        rcat = Category.objects.filter(parent=None).order_by('name')
        context['root_categories'] = rcat

        return context


class SearchBarView(TemplateView):

    def post(self, request):
        if 'search_submit' in request.POST:

            if request.POST['search_text'] != '' and request.POST['search_text'] != None:
                keyword=request.POST['search_text'].strip().replace(' ','+')
                #title = request.POST['search_text'].strip().replace(' ', '+')
                #author = request.POST['search_text'].strip().replace(' ', '+')
                #title = request.POST.getlist('title')
                #author = [request.POST['author-{}'.format(q)] for q in title]
                return HttpResponseRedirect(reverse('querydata:search',kwargs={'keyword':keyword.lower(), 'page':1 }))
                #return HttpResponseRedirect(reverse('querydata:search', kwargs={'title': title.lower(), 'author': author.lower()}))
            else:
                raise Http404()
        else:
            raise Http404()


#class BookFormView(FormView):
#    form_class = BookForm
#    template_name = 'search/search_data.html'
#    success_url = '/'

#    def post(self, request, *args, **kwargs):
#        search_form = self.form_class(request.POST)
#        search_form = BookForm()
#        if search_form.is_valid():
#            search_form.save()
#            return self.render_to_response
#            (
#                self.get_context_data
#                (
#                    success=True
#                )
#            )
#        else:
#            return self.render_to_response
#            (
#                self.get_context_data
#                (
#                    search_form=search_form
#                )
#            )



class SearchResultView(TemplateView):
    template_name = 'search/compare.html'

    def get_context_data(self, **kwargs):
        context = super(SearchResultView, self).get_context_data(**kwargs)
#       params = Request(settings.MSP_URL_KEYWORD % (kwargs['keyword'].lower(),str(kwargs['page']) ) )
        params = Request('https://affiliate-api.flipkart.net/affiliate/1.0/search.json?query=potter&resultCount=6')
#       params.add_header('User-Agent','Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)')
        params.add_header('Fk-Affiliate-Id', 'chiragjai7')
        params.add_header('Fk-Affiliate-Token', 'c846bc8ae4e24267acff37b786680901')
        jsondata = urlopen(params).read()
        jsontopy = json.loads(jsondata)  # decode the json into python objects
#       count = jsontopy['count']
        try:
#           count = int(js['count'])
#           count = jsontopy['count']
            count = int(jsontopy.get('count', 0))
        except TypeError:
            count = 0
#       count = int(jsontopy.get('count', 0))
        count_pages = int(ceil(count/50.0))
        books = []
        for item in jsontopy['productInfoList']:
            product = {}
            product['title'] = item['productBaseInfoV1']['title']
#           product['url'] = settings.MSP_URL_PRODUCT % item['id']
            product['link'] = item['productBaseInfoV1']['productUrl']
            product['price'] = item['productBaseInfoV1']['flipkartSpecialPrice']['amount']
#           product['price'] = item['final_price']
#           if Book.objects.filter(title=product['title']).count() > 0:
            if Book.objects.filter(title=product['title']).count() > 0:
                product['title'] = Book.objects.get(title=product['title']).title
#               product['epid']=Book.objects.get(title=product['id']).slug
            else:
                product['title'] = (product['title'])
#               product['epid']=msp_encode(product['title'])
            try:
                product['image'] = item['productBaseInfoV1']['imageUrls']['200x200']
            except KeyError:
                product['image'] = item['image']
            books.append(product)
        context['books'] = books
#        context['keyword']=kwargs['keyword']
        context['title'] = kwargs['title']
        context['author'] = kwargs['author']
#       context['page']=page=int(kwargs['page'])
        context['count_p'] = count_pages
        context['count_num'] = count
        arr_num_p = []
        context['arr_num_p'] = arr_num_p
        return context


class ProductView(TemplateView):
    template_name = 'detail.html'

    def add_listing(self, js, store, product, category):
        try:
            if len(js['lines'][store]) > 1:
                key = js['lines'][store]['listings'].keys()[0]
                listing = js['lines'][store]['listings'][key]
                request = Request(listing['store_url'])
                html = urlopen(request).read()
                soup = BeautifulSoup(html, 'lxml')
                surl = soup.find('a', {'class': 'store-link'})['href'].split('?')[0]
                if 'gp/offer-listing' in surl:
                    surl = surl.replace('gp/offer-listing', 'dp')
                lis, created = Listing.objects.get_or_create(store=listing['store'], product=product, category=category)
                if created:
                    lis.shipping_cost = get_json(listing, 'shipping_cost')
                    lis.mrp = get_json(listing, 'mrp')
                    lis.price = get_json(listing, 'price')
                    lis.delivery_days = get_json(listing, 'delivery')
                    lis.cod = bool(int(get_json(listing, 'cod')))
                    lis.offers = get_json(listing, 'offers')
                    lis.store_url = surl
                    lis.category = category
                    lis.save()
                return lis
            else:
                return None
        except KeyError:
            return None

#class MySearchView(SearchView):
#   def get_queryset(self):
#       queryset = super(MySearchView, self).get_queryset()

#       return queryset.filter(pub_date__gte=date(2015, 1, 1))

#   def get_context_data(self, *args, **kwargs):
#       context = super(MySearchView, self).get_context_data(*args, **kwargs)

#       return render(
#           request, 'search/search_data.html',
#           {
#               'authors':authors,
#               'books':books,
#               'results':results,
#           }
#       )

#class MySearchView(SearchView):
#   template_name = 'search/search_data.html',
#   queryset = SearchQuerySet().filter(author='john')
#   form_class = QuerydataForm

#class SearchListView(SearchView):
#   paginate_by = 10

#   def get_queryset(self):
#       result = super(SearchListView, self).get_queryset()

#       query = self.request.GET.get('q')
#       if query:
#           query_list = query.split()
#           result = result.filter(
#               reduce(operator.and_,
#                   (Q(title__icontains=q) for q in query_list)) |
#               reduce(operator.and_,
#                   (Q(content__icontains=q) for q in query_list))
#           )
#       return result

class FAQView(TemplateView):
    template_name = 'faq.html'

class ContactView(TemplateView):
    template_name = 'contact.html'

class AboutView(TemplateView):
    template_name = 'text.html'

#class RobotsView(TemplateView):
#   template_name = "robots.txt"
