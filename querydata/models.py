from django.db import models
from django.db.models import CharField, DateField, ForeignKey, Model, permalink, signals
from django.template.defaultfilters import truncatechars
from django.utils.encoding import python_2_unicode_compatible
from django.apps import AppConfig
from django.db.models import Q
from django import forms
from django.forms import ModelForm

#from dynamic_scraper.models import Scraper, SchedulerRuntime
#from scrapy_djangoitem import DjangoItem
#from scrapy.item import Item, Field
#from scraper.items import BookItem

from haystack import indexes
from autoslug import AutoSlugField

import json
import urllib
#from watson import search as watson
#import select2.fields

#from searchableselect.widgets import SearchableSelect


# Create your models here.
def msp_encode(pid):
    return pid.split(':')[0] + '-' + str(int(pid.split(':')[1])*3)


def msp_decode(epid):
    return epid.split('-')[0] + ':' + str(int(epid.split('-')[1])/3)


@python_2_unicode_compatible
class Vendor(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    url = models.URLField(default=None)
    affiliateID = models.CharField(max_length=100, blank=True, null=True)
#    scraper = models.ForeignKey(Scraper, blank=True, null=True, on_delete=models.SET_NULL)
#    scraper_runtime = models.ForeignKey(SchedulerRuntime, blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):  # __unicode__ on Python 2
        return self.name

    def __unicode__(self):
        return self.name

    class Meta:
        db_table = 'vendors'


class Author(models.Model):
    author_name = models.CharField(max_length=100, unique=True, null=False)
#   book = models.ForeignKey(Book, blank=True, null=True)
#   vendor = models.ForeignKey(Vendor, blank=True, null=True)
#   price= models.CharField("Price", max_length=32, null=True)

    def __str__(self):  # __unicode__ on Python 2
        return self.author_name

    def __unicode__(self):
        return self.author_name

    def __repr__(self):
        return '<Author: %s>' % self.author_name

    def get_absolute_url(self):
        return self.author_name

    class Meta:
        db_table = 'author_book'
#        app_label = 'Author'
        verbose_name = 'Author'
        verbose_name_plural = 'Authors'


class Book(models.Model):
    title = models.CharField("Title", max_length=225)
    slug = AutoSlugField(populate_from='title', always_update=True, null=True, blank=True)
    descript = models.TextField("Description", blank=True)
    avg_rat = models.FloatField("Average Rating", default=0)
    rating_count = models.FloatField("Rating Count", default=0)
    num_page = models.IntegerField("Number Page", default=0)
    pub_date = models.DateTimeField('Date published', auto_now_add=False, blank=True, null=True)
    pub = models.CharField("Publisher", max_length=225)
    lang = models.CharField("Language", max_length=50)
    edit_info = models.CharField("Info", max_length=225)
    image_url = models.URLField("Image")
    small_url = models.URLField("Thumbnail")
    format = models.CharField("Format", max_length=50)
    isbn = models.IntegerField("ISBN", default=0)
    isbn_13 = models.IntegerField("ISBN 13", default=0)
    link = models.URLField("Link")
#    price = models.ForeignKey(Price, blank=True, null=True)
#    author = models.ForeignKey(Author, blank=True, null=True)
#    vendor = models.ForeignKey(Vendor, blank=True, null=True)
#    price = models.CharField("Price", max_length=32, blank=True, null=True)
#    checker_runtime = models.ForeignKey(SchedulerRuntime, blank=True, null=True, on_delete=models.SET_NULL)

#    author = models.ManyToManyField(Author)
#    author = select2.fields.ForeignKey(Author,
#        overlay="Choose an author...",
#        on_delete=models.CASCADE)
#    author = select2.fields.ForeignKey(Author,
#        default=None,
#        ajax=True,
#        search_field='author_name',
#        search_field=lambda q: Q(author_name__icontains=q) | Q(alt_name__icontains=q),
#        overlay="Choose an author...",
#        js_options={
#            'quiet_millis': 200,
#        },
#        on_delete=models.CASCADE)
#    authors = select2.fields.ManyToManyField(Author)

#    @property
#    def short_description(self):
#        return truncatechars(self.descript, 100)
    def __str__(self):  # __unicode__ on Python 2
        return self.title

    def __unicode__(self):
        return self.title

    def __repr__(self):
        return '<Book: %s>' % self.title

#    @classmethod
#    def get_flipkart(cls, price):
#        params = {'url': self.url, 'format': 'json'}
#        fetch_url = 'https://affiliate-api.flipkart.net/affiliate/1.0/search.json?query=%s&resultCount=6' % urllib.urlencode(params)
#        fetch_url = 'https://www.flipkart.com/books/pr?sid=bks&q=%s&otracker=categorytree' % urllib.urlencode(params)
#        result = urllib.urlopen(fetch_url).read()
#        result = json.loads(result)
#        KEYS = ['title', 'type', 'url', 'description', 'provider_url', 'provider_name', 'width', 'height', 'html', 'thumbnail_url', 'author_url']
#        for key in KEYS:
#            if result in(key):
#                setattr(self, key, result[key])

#    def save(self, *args, **kwargs):
#        if not self.pk:
#            self.get_flipkart()
#        super(Book, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return self.title

    class Meta:
        db_table = 'books'
#       get_latest_by = 'author_name'
#       ordering = ('author_name',)


#class BookItem(DjangoItem):
#    django_model = Book

class Search(models.Model):
    title = models.ForeignKey(Book, blank=True, null=True, related_name='title')
    author = models.ForeignKey(Author, blank=True, null=True, related_name='author')
    price = models.CharField("Price", max_length=32, blank=True, null=True, related_name='price')
    vendor = models.ForeignKey(Vendor, blank=True, null=True, related_name='vendor')

    def __str__(self):  # __unicode__ on Python 2
        return self.title

    def __unicode__(self):
        return self.title

    def __repr__(self):
        return '<Book: %s>' % self.title

    def get_absolute_url(self):
        return self.title

    class Meta:
        db_table = 'search'
#       get_latest_by = 'author_name'
#       ordering = ('author_name',)


class Employee(models.Model):
    first_name = models.CharField(max_length=225)
    last_name = models.CharField(max_length=225)

    class Meta:
        db_table = 'employees'


class Rating(models.Model):
    five_r = models.CharField(max_length=20)
    four_r = models.CharField(max_length=20)
    three_r = models.CharField(max_length=20)
    two_r = models.CharField(max_length=20)
    one_r = models.CharField(max_length=20)
    total_r = models.CharField(max_length=20)

    class Meta:
        db_table = 'ratings'


class SimilarBook(models.Model):
    id_o = models.CharField(max_length=15)
    id_d = models.CharField(max_length=15)

    class Meta:
        db_table = 'similar_book'
