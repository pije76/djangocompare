from __future__ import unicode_literals

from django import forms
from django.forms import ModelForm, ModelChoiceField
from django.utils.translation import ugettext_lazy as _

import select2rocks

from models import Book, Author, Search

class BookForm(forms.Form):
    title = forms.CharField(label='')

#    class Meta:
#        model = Book
#        exclude = ('author',)
#        fields = ('title',)
#        labels = {'title': ''}

    def __init__(self, *args, **kwargs):
        super(BookForm, self).__init__(*args, **kwargs)
        self.fields['title'].required = True
        self.fields['title'].label = ""
        self.fields['title'].error_messages = {'required': ''}
#        self.fields['title'].widget = forms.HiddenInput()
        self.fields['title'].widget.attrs.update({
            'id': 'id_searchbar',
            'class': 'form-control',
            'style': 'width:300px;color:#000;font-size:16px;',
            'name': 'title',
            'placeholder': 'Search Title'
        })


class AuthorForm(forms.Form):
#    author = forms.ModelChoiceField(label='', queryset=Author.objects.all())
#    author = forms.ModelChoiceField(label='', queryset=Author.objects.filter(author_name__author=author))
#    author = forms.ModelChoiceField(label='', queryset=Author.objects.get(author_name__author=author))
    author = forms.CharField(label='')
#    author = forms.ChoiceField(label='', queryset=Author.objects.all())

#    class Meta:
#        model = Author
#        fields = ('author_name',)
#        exclude = ()
#        labels = {'author_name': ''}
#        widgets = {
#            'name': Textarea(attrs={'cols': 80, 'rows': 20}),
#        }

    def __init__(self, *args, **kwargs):
        super(AuthorForm, self).__init__(*args, **kwargs)
#        author = Author.objects.filter(author_name=author_name)
#        self.fields['author'].queryset = author
        self.fields['author'].error_messages = {'required': ''}
        self.fields['author'].empty_label = 'Please Select Author'
        self.fields['author'].label = ""
        self.fields['author'].widget.attrs.update({
            'id': 'id_searchbar',
            'class': 'form-control',
#            'type': 'text',
            'style': 'width:250px;color:#000;font-size:16px;',
            'name': 'author',
            'placeholder': 'Search Author'
        })
#        widgets = {
#            'author': SearchableSelect(model='querydata.Author', search_field='author_name', many=False, limit=20),
#        }

class SearchForm(forms.ModelForm):
#    title = forms.ModelChoiceField(label='', queryset=Book.objects.all())
#    author = forms.ModelChoiceField(label='', queryset=Author.objects.all())

    class Meta:
        model = Search
#        exclude = ('author',)
        fields = ('title','author')
        labels = {'title': '','author': ''}

    title = select2rocks.Select2ModelChoiceField(
        queryset=Search.objects.all(),
        widget=select2rocks.AjaxSelect2Widget(
            url_name='title',
            select2_options={
                'placeholder': _("Select a title"),
                'ajax': {
                    'quietMillis': 50
                }
            }))

    author = select2rocks.Select2ModelChoiceField(
        queryset=Search.objects.all(),
        widget=select2rocks.AjaxSelect2Widget(
            url_name='author',
            select2_options={
                'placeholder': _("Select a author"),
                'ajax': {
                    'quietMillis': 50
                }
            }))

    def __init__(self, *args, **kwargs):
        super(SearchForm, self).__init__(*args, **kwargs)

#        self.fields['title'].queryset = title
        self.fields['title'].error_messages = {'required': ''}
        self.fields['title'].empty_label = 'Please Select Book'
#        self.fields['title'].label = ""
        self.fields['title'].widget.attrs.update({
            'id': 'id_searchbar',
            'class': 'form-control',
            'style': 'width:300px;color:#000;font-size:16px;',
            'name': 'title',
            'placeholder': 'Search Title'
        })
#        self.fields['author'].queryset = author
        self.fields['author'].error_messages = {'required': ''}
        self.fields['author'].empty_label = 'Please Select Author'
        self.fields['author'].label = ""
        self.fields['author'].widget.attrs.update({
            'id': 'id_searchbar',
            'class': 'form-control',
#            'type': 'text',
            'style': 'width:250px;color:#000;font-size:16px;',
            'name': 'author',
            'placeholder': 'Search Author'
        })
