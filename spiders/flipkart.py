from bs4 import BeautifulSoup
import re
try:
    from urllib.request import urlopen
except ImportError:
    from urllib2 import Request, urlopen
import requests
import json


class FlipkartSpider(object):
    def __init__(self, domain=''):
        self.start_urls = domain
        self.soup_list = []

    def __str__(self):
        return 'Flipkart'

    def parse(self):

        req = requests.get(self.start_urls).content

        self.soup_list.append(BeautifulSoup(req, 'lxml'))

        while len(self.soup_list) < 5:
            try:
                new_soup = self.get_next_page(self.soup_list[-1])
                self.soup_list.append(new_soup)
            except:
                break

    def scrape(self):
        for page in self.page_list:

            for game in page['products']:

                deal = {}

                deal['platforms'] = '/'
                deal['store'] = 'Flipkart'
                deal['storelink'] = 'https://www.flipkart.com/'
                deal['title'] = str(game['title'])
                deal['faketitle'] = re.sub(r'[^\w]', '', deal['title']).lower()
                deal['link'] = str(game['smartUrl'])
                deal['original_price'] = float(game['prices']['MRP'])
                deal['price'] = str(game['finalPrice'])

                yield deal
