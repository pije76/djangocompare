from threading import Thread
import re
try:
    from urllib.request import quote
except ImportError:
    from urllib import quote
from requests.exceptions import RequestException
# Importing the spiders

from .flipkart import FlipkartSpider
from .amazon import AmazonSpider
from .infibeam import InfibeamSpider
from .snapdeal import SnapdealSpider
from .ebay import EbaySpider


def set_domains(key):
    domain_flipkart = ''
#    domain_amazon = ''
#    domain_gmg = ''
    linkWords = [quote(word) for word in key.split()]
    print (linkWords)
    for word in linkWords:
        domain_flipkart += word + '+'
#        domain_gmg += word + '%20'
#    domain_infibeam = domain_flipkart
#    domain_snapdeal = domain_flipkart
#    domain_ebay = domain_flipkart

#    domain_gmg = 'https://www.greenmangaming.com/search/' + domain_gmg[:-3]+'?platform=73'

    domain_flipkart = 'https://www.flipkart.com/books/pr?sid=bks&q=' + domain_flipkart[:-1]
#    domain_flipkart = 'https://affiliate-api.flipkart.net/affiliate/1.0/search.json?query=' + domain_flipkart[:-1]+'&resultCount=10'
#    domain_amazon = 'https://www.amazon.com/s/ref=nb_sb_ss_i_1_12/142-3222807-6684620?url=search-alias%3Dstripbooks&field-keywords=' + domain_flipkart[:-1]
#    domain_infibeam = 'https://www.infibeam.com/Books/search?q=harry%20potter' + domain_gmg[:-1]
#    domain_snapdeal = 'https://www.snapdeal.com/search?keyword=harry%20potter&santizedKeyword=' + domain_gmg[:-3]+'&catId=0&categoryId=364&suggested=false&vertical=p&noOfResults=20'
#    domain_ebay = 'https://www.ebay.com/sch/Books/267/i.html?_from=R40&_nkw=' + domain_flipkart[:-1]

    return domain_flipkart


def is_sublist(input_key, title):
    for word in input_key:
        if word not in title:
            return False
    return True


def filter(key, game_list):
    filtered_list = []
    key_input = key.lower().split()
    for game in game_list:
        title = re.sub(r'[^\w]', ' ', game['title']).lower().split()
        if is_sublist(key_input, title):
            filtered_list.append(game)
    return filtered_list


class SpiderThread(Thread):
    def __init__(self, spidername):
        Thread.__init__(self)
        self.spider = spidername

    def run(self):
        try:
            self.spider.parse()
        except RequestException as e:
            print (e)
            offline.append(self.spider)


def set_spiders(key, excluded):

    domains = set_domains(key)

    flipkart_game = FlipkartSpider(domains[0])
#    amazon_game = AmazonSpider(domains[1])
#    infibeam_game = InfibeamSpider(domains[2])
#    snapdeal_game = SnapdealSpider(domains[3])
#    domain_ebay = EbaySpider(domains[4])
    spiders_filtered = [flipkart_game, ]
    spiders_not_filtered = [flipkart_game, ]
    if excluded:
        spiders_filtered = [spider for spider in spiders_filtered if str(spider) not in excluded]
        spiders_not_filtered = [spider for spider in spiders_not_filtered if str(spider) not in excluded]

    return spiders_filtered, spiders_not_filtered


def run_spiders(key, excluded):

    global offline
    offline = []
    threads = []

    spiders_filtered, spiders_not_filtered = set_spiders(key, excluded)
    spiders = spiders_filtered + spiders_not_filtered
    for spider in spiders:
        t = SpiderThread(spider)
        t.start()
        threads.append(t)
    for t in threads:
        t.join()

    # progress.delete() # The progress bar is not needed anymore as it reached 100%
    results = []
    results_filtered = []
    for spider in spiders_not_filtered:
        if spider not in offline:
            try:
                results.append(list(filter(key, spider.scrape())))
            except Exception as e:
                print(e)
                offline.append(spider)
    for spider in spiders_filtered:
        if spider not in offline:
            try:
                results_filtered.append(list(filter(key, spider.scrape())))
            except Exception as e:
                print(e)
                offline.append(spider)
    # results = [list(filter(key,spider.scrape())) for spider in spiders_not_filtered]
    # results_filtered = [list(spider.scrape()) for spider in spiders_filtered]
    results += results_filtered
    return results, offline
