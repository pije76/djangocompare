"""bookshopper URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import *
from django.contrib import admin
from django.conf import settings

from haystack.views import SearchView

#from sastakart.views import *
#from shop.views import *
from querydata.views import *

urlpatterns = [
#    url(r'^', include('django.contrib.auth.urls')),
#    url(r'^', include('sastakart.urls',namespace='sastakart')),
#    url(r'^$', home, name='home'),
#    url(r'^search/$', search, name='search'),
#    url(r'^$', include('searchableselect.urls')),
#    url(r'',include('querydata.urls')),
    url(r'^', include('querydata.urls',namespace='querydata')),

#    url('^search/', include('searchableselect.urls')),
#    url(r'^search/', include('select2.urls')),
#    url(r'^search/', search, name='search'),
#    url(r'^search/', include('haystack.urls')),
#    url(r'^(?P<cat_slug>[-\w]+)/(?P<asin>[-\w]{10})/$', product_page, name='product_page'),
#    url(r'^(?P<slug>[-\w]+)/$', category_view, name='category_view'),
#    url(r'^page/(?P<slug>[-\w]+)/$', static_page, name='static_page'),
#    url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
    url(r'^admin/', include(admin.site.urls)),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
